package lt.insoft.fileup.facades;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import lt.insoft.fileup.model.UploadedFile;
import lt.insoft.fileup.model.UploadedFile_;

import org.springframework.transaction.annotation.Transactional;

@Transactional
public class FileFacadeImpl implements FileFacade {

	
	protected EntityManager entityManager;
	
	
	@Override
	@Transactional
	public void saveFile(UploadedFile file) {
		entityManager.persist(file);
	}


	@Override
	@Transactional
	public List<UploadedFile> getFile(int id) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<UploadedFile> query = builder.createQuery(UploadedFile.class);
		Root<UploadedFile> root = query.from(UploadedFile.class);
		Predicate condition = builder.equal(root.get(UploadedFile_.id), id);
		
		query.where(condition);
		TypedQuery<UploadedFile> typedQuery = entityManager.createQuery(query);
		List<UploadedFile> res = typedQuery.getResultList();
		return res;
	}

	@Override
	@Transactional
	public List<UploadedFile> getFiles() {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<UploadedFile> query = builder.createQuery(UploadedFile.class);
		Root<UploadedFile> root = query.from(UploadedFile.class);
		Predicate condition = builder.isNotNull(root.get(UploadedFile_.id));
		
		query.where(condition);
		TypedQuery<UploadedFile> typedQuery = entityManager.createQuery(query);
		List<UploadedFile> res = typedQuery.getResultList();
		return res;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<UploadedFile> searchFiles(String searchQuery) {
		Query query = entityManager.createNativeQuery(
				"select *"
				+ "from UPLOADED_FILE "
				+ "where dbms_lob.instr(TEXT, ?1)>0", UploadedFile.class);
		query.setParameter(1, searchQuery);
		query.executeUpdate();
		System.out.println(query.getResultList().size());
		List<UploadedFile> res = query.getResultList();
		return (List<UploadedFile>) res;
	}

	@Override
	@Transactional
	public void removeFile(UploadedFile file) {
		UploadedFile f = entityManager.find(UploadedFile.class, file.getId());
		entityManager.remove(f);
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}

	@PersistenceContext
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
}
