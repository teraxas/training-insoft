package lt.insoft.fileup.utilities;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import lt.insoft.fileup.model.UploadedFile;

import org.apache.commons.compress.utils.IOUtils;
import org.apache.tika.Tika;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;

public class UploadedFileCreator {
	
	
	static public String parseDocument(InputStream stream, String filename){
		Tika tika = new Tika();
		
		Metadata metadata = new Metadata();
		metadata.set(Metadata.RESOURCE_NAME_KEY, filename);
		
		
		
		String text;
		try {
			text = tika.parseToString(stream, metadata);
		} catch (IOException | TikaException e) {
			text = "";
			e.printStackTrace();
		}
		
		return text;
	}
	
	static public String parseDocument(File f){
		Tika tika = new Tika();
		String text;
		
		try {
			text = tika.parseToString(f);
			f.delete();
		} catch (IOException | TikaException e) {
			text = "";
			e.printStackTrace();
		}
		
		return text;
	}
	
	
	static public UploadedFile createUplodedFile(
			String filename, InputStream inputStream, String contentType) throws IOException {

		
		byte[] content = IOUtils.toByteArray(inputStream);
		File tmpFile = bytesToFile(content, filename);
		
//		String text = parseDocument(inputStream, filename);
		String text = parseDocument(tmpFile);
		
		UploadedFile uf = new UploadedFile(filename, content , text, contentType);
		return uf;
	}

	
	static public File streamToFile(InputStream stream, String filename) throws IOException{
		File file = File.createTempFile("tmp", filename);
		FileOutputStream fos = new FileOutputStream(file);
		
		int read = 0;
		byte[] bytes = new byte[1024];
		while ((read = stream.read(bytes)) != -1) {
			fos.write(bytes, 0, read);
		}
		fos.close();
		return file;
	}
	
	static public File bytesToFile(byte[] bytes, String filename) throws IOException{
		File f = File.createTempFile("tmp", filename);
		FileOutputStream fos = new FileOutputStream(f);
		
		fos.write(bytes);
		fos.close();
		return f;
	}
	
	
}




