package lt.insoft.fileup.facades;

import java.util.List;

import lt.insoft.fileup.model.UploadedFile;


public interface FileFacade {

	
	public void saveFile(UploadedFile file);
	
	public List<UploadedFile> getFile(int id);
	
	public List<UploadedFile> getFiles();
	
	public void removeFile(UploadedFile file);

	public List<UploadedFile> searchFiles(String searchQuery);
	
}
