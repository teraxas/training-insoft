package lt.insoft.fileup.vm;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import lt.insoft.fileup.facades.FileFacade;
import lt.insoft.fileup.model.UploadedFile;
import lt.insoft.fileup.utilities.UploadedFileCreator;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.Media;
import org.zkoss.zhtml.Filedownload;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.annotation.WireVariable;


public class FileVM {



	@WireVariable
	private FileFacade fileFacade;

	private List<UploadedFile> fileList;

	private UploadedFile selectedFile;

	@Init
	@NotifyChange("fileList")
	public void init() {
		updateList();
	}

	@Command("upload")
	@NotifyChange("fileList")
	public void upload(@BindingParam("upEvent") UploadEvent event) {
		Media media = event.getMedia();
		String filename = media.getName();
		InputStream inputStream = media.getStreamData();
		String contentType = media.getContentType();

		
//		boolean inMemory =  media.inMemory();
//		boolean isBinary = media.isBinary();
//		
//		if (inMemory) {
//			if (isBinary)
//				media.getByteData();
//			else
//				media.getStringData();
//		} else {
//			if (isBinary)
//				media.getStreamData();
//			else
//				media.getReaderData();
//		}

		UploadedFile uf = null;

		try {
			uf = UploadedFileCreator.createUplodedFile(filename, inputStream, contentType);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}

		fileFacade.saveFile(uf);
		System.out.println(uf.toString());
		updateList();
	}
	
	@Command("download")
	public void downloadSelectedFile(){
		if (selectedFile == null)
			return;
		
		Filedownload.save(selectedFile.getContent(), "", 
				selectedFile.getFilename());
	}
	
	@Command("delete")
	@NotifyChange("fileList")
	public void deleteSelectedFile(){
		if (selectedFile == null)
			return;
		
		fileFacade.removeFile(selectedFile);
		updateList();
	}
	
	@Command("search")
	@NotifyChange("fileList")
	public void searchFiles(@BindingParam("query") String searchQuery){
		fileList = fileFacade.searchFiles(searchQuery);
		System.out.println("Searching: " + searchQuery);
	}
	
	@Command("refresh")
	@NotifyChange("fileList")
	public void updateList() {
		List<UploadedFile> newList = fileFacade.getFiles();
		if (newList != null) {
			fileList = newList;
		}
	}

	public FileFacade getFileFacade() {
		return fileFacade;
	}

	public void setFileFacade(FileFacade fileFacade) {
		this.fileFacade = fileFacade;
	}

	public List<UploadedFile> getFileList() {
		return fileList;
	}

	public void setFileList(List<UploadedFile> fileList) {
		this.fileList = fileList;
	}

	public UploadedFile getSelectedFile() {
		return selectedFile;
	}

	public void setSelectedFile(UploadedFile selectedFile) {
		this.selectedFile = selectedFile;
	}

}
