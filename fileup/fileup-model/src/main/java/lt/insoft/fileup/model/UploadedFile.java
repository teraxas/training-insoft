package lt.insoft.fileup.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table (name = "UPLOADED_FILE")
public class UploadedFile implements Serializable{

	private static final long serialVersionUID = -6795148017167065540L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GEN_UPLOADED_FILE")
	@SequenceGenerator(name = "GEN_UPLOADED_FILE", sequenceName = "SEQ_UPLOADED_FILE")
	@Column (name = "ID")
	private Long id;
	
	@Column (name = "FILENAME")
	private String filename;
	
	@Lob
	@Column (name = "CONTENT")
	@Basic (fetch = FetchType.LAZY)
	private byte[] content;
	
	@Lob
	@Column (name = "TEXT")
	@Basic (fetch = FetchType.LAZY)
	private String text;
	
	
	public UploadedFile() {
		
	}
	
	public UploadedFile(String filename, 
			byte[] content, String text, String contentType) {
		this.content = content;
		this.filename = filename;
		this.text = text;
	}
	
	@Override
	public String toString() {
		String str = id + ": " + filename + ", " + content.length +
				" (parsed length: " + ")";
		return str;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
