package lt.insoft.fileup.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2013-08-13T13:16:56.484+0300")
@StaticMetamodel(UploadedFile.class)
public class UploadedFile_ {
	public static volatile SingularAttribute<UploadedFile, Long> id;
	public static volatile SingularAttribute<UploadedFile, String> filename;
	public static volatile SingularAttribute<UploadedFile, byte[]> content;
	public static volatile SingularAttribute<UploadedFile, String> text;
}
